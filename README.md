Tabbed interface UI for the iDiscover service
=============================================

Tabbed interface implemented for the iDiscover service (see http://idiscover.lib.cam.ac.uk/). Generated from https://bitbucket.org/CUDL/searchbar-tabs.


## Usage

**Do not** edit the contents of this repo directly. The contents in the `js` and `css` folders have to be generated from the [searchbar-tabs repo](https://bitbucket.org/CUDL/searchbar-tabs).
